#include "hrac.h"

hrac::hrac(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	srand(time(NULL));

	q[0] = Question("V akej pribliznej vlnovej dlzke je viditelne svetlo?", "100 az 300 nm", "1400 az 1700 nm", "400 az 700 nm", "1400 az 1700 m");
	q[1] = Question("Piaty najvacsi ostrov je?", "Baffinov ostrov", "Mallorca", "Australia", "Velka Britania");
	q[2] = Question("Ktoru planetu pozname pod nazvom Vecernica?", "Mars", "Jupiter", "Venusa", "Merkur");
	q[3] = Question("Kolko je studentov MPM od 1. po 5. rocnik?", "44", "45", "46", "47");
	q[4] = Question("Format papiera oznacovany ako A0 ma plochu?", "0,25m", "0,5m", "1m", "1,5m");
	q[5] = Question("Ludske telo odbura 1 promile alkoholu za?","1 hodinu", "5 hodin", "10 hodin", "15 hodin");
	q[6] = Question("Kazdy stvrty clovek v Europe trpi alergiou. Najcastejsim typom je alergia?", "na pupavy", "na pel", "na travu", "na prach");
	q[7] = Question("Ktora kniha nepatri do trilogie Pan Prstenov?", "Dve veze", "Navrat Krala", "Spolocenstvo prstena", "Kamen mudrcov");
	q[8] = Question("Poruchy prijmu potravy sa v Medzinarodnej klasifikacii chorob oznacuju ako diagnoza?", "D89", "F50", "C48", "M14");
	q[9] = Question("Eulerovo cislo je?", "komplexne", "iracionalne", "imaginarne", "racionalne");
	
	q[0].nastavOdpoved(3);
	q[1].nastavOdpoved(1);
	q[2].nastavOdpoved(3);
	q[3].nastavOdpoved(1);
	q[4].nastavOdpoved(3);
	q[5].nastavOdpoved(3);
	q[6].nastavOdpoved(4);
	q[7].nastavOdpoved(4);
	q[8].nastavOdpoved(2);
	q[9].nastavOdpoved(2);

	ui.pushButton_2->setDisabled(true);
	
	ui.groupBox_2->setDisabled(true);
	ui.groupBox_3->setDisabled(true);
	ui.groupBox_4->setDisabled(true);
	ui.groupBox_5->setDisabled(true);

}

void hrac::on_pushButton_clicked() //nova hra
{
	p.nastavMeno(ui.lineEdit->text());

	if (ui.lineEdit->text() == "")
	{
		mBox.setText("Najprv zadaj meno");
		mBox.exec();
	}

	else
	{
		ui.pushButton->setDisabled(true);
		ui.pushButton_2->setDisabled(false);
		
		ui.groupBox_2->setDisabled(false);
		ui.groupBox_3->setDisabled(false);
		ui.groupBox_4->setDisabled(false);
		ui.groupBox_5->setDisabled(false);
		
		ui.comboBox->setDisabled(true);
		ui.checkBox->setDisabled(true);
		
		if (ui.checkBox->isChecked())
			std::random_shuffle(std::begin(q), std::end(q));

		if (ui.comboBox->currentIndex() == 0)
		{
			ui.pushButton_3->setVisible(true);
			ui.pushButton_4->setVisible(true);
			ui.pushButton_5->setVisible(true);
		}

		if (ui.comboBox->currentIndex() == 1)
		{
			ui.pushButton_3->setVisible(true);
			ui.pushButton_4->setVisible(true);
			ui.pushButton_5->setVisible(false);
		}

		if (ui.comboBox->currentIndex() == 2)
		{
			ui.pushButton_3->setVisible(true);
			ui.pushButton_4->setVisible(false);
			ui.pushButton_5->setVisible(false);
		}
		
		ui.spinBox->setValue(s + 1);
		ui.textEdit->setText(q[s].dajOtazku());
		ui.radioButton->setText(q[s].dajMoznost(0));
		ui.radioButton_2->setText(q[s].dajMoznost(1));
		ui.radioButton_3->setText(q[s].dajMoznost(2));
		ui.radioButton_4->setText(q[s].dajMoznost(3));
		s++;
	}
}

void hrac::on_pushButton_2_clicked() //ukoncit
{
	ui.groupBox->setDisabled(true);
	ui.groupBox_2->setDisabled(true);
	ui.groupBox_3->setDisabled(true);
	ui.groupBox_4->setDisabled(true);
	ui.groupBox_5->setDisabled(true);

	mBox.setText("Hra je ukoncena");
	mBox.exec();
}

void hrac::on_pushButton_3_clicked()//zolik1
{
	ui.pushButton_3->setDisabled(true);
	
	int r = 1;

	while (r == q[s - 1].dajOdpoved())
		r = rand() % 4 + 1;
	
	ui.radioButton->setDisabled(true);
	ui.radioButton_2->setDisabled(true);
	ui.radioButton_3->setDisabled(true);
	ui.radioButton_4->setDisabled(true);

	if (q[s - 1].dajOdpoved() == 1)
		ui.radioButton->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 2)
		ui.radioButton_2->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 3)
		ui.radioButton_3->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 4)
		ui.radioButton_4->setDisabled(false);

	if (r == 1)
		ui.radioButton->setDisabled(false);
	if (r == 2)
		ui.radioButton_2->setDisabled(false);
	if (r == 3)
		ui.radioButton_3->setDisabled(false);
	if (r == 4)
		ui.radioButton_4->setDisabled(false);

}

void hrac::on_pushButton_4_clicked()//zolik2
{
	ui.pushButton_4->setDisabled(true);

	int r = 1;

	while (r == q[s - 1].dajOdpoved())
		r = rand() % 4 + 1;

	ui.radioButton->setDisabled(true);
	ui.radioButton_2->setDisabled(true);
	ui.radioButton_3->setDisabled(true);
	ui.radioButton_4->setDisabled(true);

	if (q[s - 1].dajOdpoved() == 1)
		ui.radioButton->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 2)
		ui.radioButton_2->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 3)
		ui.radioButton_3->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 4)
		ui.radioButton_4->setDisabled(false);

	if (r == 1)
		ui.radioButton->setDisabled(false);
	if (r == 2)
		ui.radioButton_2->setDisabled(false);
	if (r == 3)
		ui.radioButton_3->setDisabled(false);
	if (r == 4)
		ui.radioButton_4->setDisabled(false);
}

void hrac::on_pushButton_5_clicked()//zolik3
{
	ui.pushButton_5->setDisabled(true);

	int r = 1;

	while (r == q[s - 1].dajOdpoved())
		r = rand() % 4 + 1;

	ui.radioButton->setDisabled(true);
	ui.radioButton_2->setDisabled(true);
	ui.radioButton_3->setDisabled(true);
	ui.radioButton_4->setDisabled(true);

	if (q[s - 1].dajOdpoved() == 1)
		ui.radioButton->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 2)
		ui.radioButton_2->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 3)
		ui.radioButton_3->setDisabled(false);
	if (q[s - 1].dajOdpoved() == 4)
		ui.radioButton_4->setDisabled(false);

	if (r == 1)
		ui.radioButton->setDisabled(false);
	if (r == 2)
		ui.radioButton_2->setDisabled(false);
	if (r == 3)
		ui.radioButton_3->setDisabled(false);
	if (r == 4)
		ui.radioButton_4->setDisabled(false);
}

void hrac::on_pushButton_6_clicked() //potvrdit
{
	if (ui.radioButton->isChecked() && q[s - 1].dajOdpoved() == 1) 
	{
		mBox.setText("Spravna odpoved");
		mBox.exec();
		p.plus();
	}

	else if (ui.radioButton_2->isChecked() && q[s - 1].dajOdpoved() == 2)
	{
		mBox.setText("Spravna odpoved");
		mBox.exec();
		p.plus();
	}

	else if (ui.radioButton_3->isChecked() && q[s - 1].dajOdpoved() == 3)
	{
		mBox.setText("Spravna odpoved");
		mBox.exec();
		p.plus();
	}

	else if (ui.radioButton_4->isChecked() && q[s - 1].dajOdpoved() == 4)
	{
		mBox.setText("Spravna odpoved");
		mBox.exec();
		p.plus();
	}

	else
	{
		mBox.setText("Nespravna odpoved");
		mBox.exec();
		p.minus();
	}

	ui.doubleSpinBox->setValue(p.vypisSkore());

	ui.radioButton->setDisabled(false);
	ui.radioButton_2->setDisabled(false);
	ui.radioButton_3->setDisabled(false);
	ui.radioButton_4->setDisabled(false);

	if (s == 10) 
		on_pushButton_2_clicked();

	else on_pushButton_clicked();

}

void hrac::on_pushButton_7_clicked() //preskocit otazku
{
	p.preskoc();
	ui.doubleSpinBox->setValue(p.vypisSkore());
	s++;
	on_pushButton_clicked();

	ui.radioButton->setDisabled(false);
	ui.radioButton_2->setDisabled(false);
	ui.radioButton_3->setDisabled(false);
	ui.radioButton_4->setDisabled(false);
}