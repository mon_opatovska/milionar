/********************************************************************************
** Form generated from reading UI file 'hrac.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HRAC_H
#define UI_HRAC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_hracClass
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QLabel *label_3;
    QComboBox *comboBox;
    QCheckBox *checkBox;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QDoubleSpinBox *doubleSpinBox;
    QGroupBox *groupBox_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QGroupBox *groupBox_3;
    QTextEdit *textEdit;
    QSpinBox *spinBox;
    QGroupBox *groupBox_4;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QGroupBox *groupBox_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *hracClass)
    {
        if (hracClass->objectName().isEmpty())
            hracClass->setObjectName(QStringLiteral("hracClass"));
        hracClass->resize(760, 490);
        centralWidget = new QWidget(hracClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 731, 41));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 47, 16));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(40, 10, 113, 20));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(170, 10, 47, 16));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(290, 10, 47, 16));
        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(330, 10, 69, 22));
        checkBox = new QCheckBox(groupBox);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(420, 10, 70, 21));
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(520, 10, 75, 23));
        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(610, 10, 75, 23));
        doubleSpinBox = new QDoubleSpinBox(groupBox);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(200, 10, 62, 22));
        doubleSpinBox->setReadOnly(true);
        doubleSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox->setMinimum(-10);
        doubleSpinBox->setMaximum(10);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 70, 731, 61));
        pushButton_3 = new QPushButton(groupBox_2);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(200, 30, 75, 23));
        pushButton_4 = new QPushButton(groupBox_2);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(310, 30, 75, 23));
        pushButton_5 = new QPushButton(groupBox_2);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(420, 30, 75, 23));
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 150, 731, 111));
        textEdit = new QTextEdit(groupBox_3);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 50, 711, 41));
        textEdit->setReadOnly(true);
        spinBox = new QSpinBox(groupBox_3);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(10, 20, 42, 22));
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(10, 280, 731, 71));
        radioButton = new QRadioButton(groupBox_4);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(10, 30, 181, 21));
        radioButton_2 = new QRadioButton(groupBox_4);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(200, 30, 171, 21));
        radioButton_3 = new QRadioButton(groupBox_4);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setGeometry(QRect(380, 30, 171, 21));
        radioButton_4 = new QRadioButton(groupBox_4);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setGeometry(QRect(561, 30, 161, 20));
        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 370, 731, 51));
        pushButton_6 = new QPushButton(groupBox_5);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(500, 20, 75, 23));
        pushButton_7 = new QPushButton(groupBox_5);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(610, 20, 101, 23));
        hracClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(hracClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 760, 21));
        hracClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(hracClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        hracClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(hracClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        hracClass->setStatusBar(statusBar);

        retranslateUi(hracClass);

        QMetaObject::connectSlotsByName(hracClass);
    } // setupUi

    void retranslateUi(QMainWindow *hracClass)
    {
        hracClass->setWindowTitle(QApplication::translate("hracClass", "hrac", 0));
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("hracClass", "Meno", 0));
        label_2->setText(QApplication::translate("hracClass", "Sk\303\263re", 0));
        label_3->setText(QApplication::translate("hracClass", "\303\232rove\305\210", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("hracClass", "\304\275ahk\303\241", 0)
         << QApplication::translate("hracClass", "Stredn\303\241", 0)
         << QApplication::translate("hracClass", "\305\244a\305\276k\303\241", 0)
        );
        checkBox->setText(QApplication::translate("hracClass", "N\303\241hodne", 0));
        pushButton->setText(QApplication::translate("hracClass", "Nov\303\241 hra", 0));
        pushButton_2->setText(QApplication::translate("hracClass", "Ukon\304\215i\305\245 hru", 0));
        groupBox_2->setTitle(QApplication::translate("hracClass", "\305\275ol\303\255ky", 0));
        pushButton_3->setText(QApplication::translate("hracClass", "50:50", 0));
        pushButton_4->setText(QApplication::translate("hracClass", "50:50", 0));
        pushButton_5->setText(QApplication::translate("hracClass", "50:50", 0));
        groupBox_3->setTitle(QApplication::translate("hracClass", "Ot\303\241zka", 0));
        groupBox_4->setTitle(QApplication::translate("hracClass", "Mo\305\276nosti", 0));
        radioButton->setText(QApplication::translate("hracClass", "A)", 0));
        radioButton_2->setText(QApplication::translate("hracClass", "B)", 0));
        radioButton_3->setText(QApplication::translate("hracClass", "C)", 0));
        radioButton_4->setText(QApplication::translate("hracClass", "D)", 0));
        groupBox_5->setTitle(QString());
        pushButton_6->setText(QApplication::translate("hracClass", "Potvrdi\305\245", 0));
        pushButton_7->setText(QApplication::translate("hracClass", "Presko\304\215i\305\245 ot\303\241zku", 0));
    } // retranslateUi

};

namespace Ui {
    class hracClass: public Ui_hracClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HRAC_H
