#ifndef HRAC_H
#define HRAC_H

#include <QtWidgets/QMainWindow>
#include "ui_hrac.h"
#include <QMessageBox>
#include <QString>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

class Player
{
private:
	QString meno;
	double skore = 0.0;

public:
	Player() {}

	void nastavMeno(QString m) { meno = m; }
		
	void plus() { skore++; }
	void minus() { skore--;}
	void preskoc() { skore = skore - 0.5; }
	double vypisSkore() { return skore; }
};

class Question
{
private:
	QString otazka;
	QString moznost[4];
	int odpoved;

public:
	Question() {}
	Question(QString o, QString m0, QString m1, QString m2, QString m3) 
	{
		otazka = o;
		moznost[0] = m0;
		moznost[1] = m1;
		moznost[2] = m2;
		moznost[3] = m3;
	}
	void nastavOdpoved(int x) { odpoved = x; }

	QString dajOtazku() { return otazka; }
	QString dajMoznost(int i) { return moznost[i]; }
	int dajOdpoved() { return odpoved; }

};



class hrac : public QMainWindow
{
	Q_OBJECT

public:
	hrac(QWidget *parent = 0);
	
	QMessageBox mBox;
	Question q[10];
	Player p;

	int s = 0;

private:
	Ui::hracClass ui;

private slots:
	void on_pushButton_clicked();
	void on_pushButton_2_clicked();
	void on_pushButton_3_clicked();
	void on_pushButton_4_clicked();
	void on_pushButton_5_clicked();
	void on_pushButton_6_clicked();
	void on_pushButton_7_clicked();

};

#endif // HRAC_H
